import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './Header.css';

class Header extends Component {
    render () {
        return (
            <div>
                <nav className="top-nav-bar">
                    <NavLink className="top-nav-bar-item mdc-typography--button" to="/overview" activeClassName="active">Overview</NavLink>
                    <NavLink className="top-nav-bar-item mdc-typography--button" to="/report-shortcuts" activeClassName="active">Report Shortcuts</NavLink>
                </nav>
            </div>
        )
    }
} 

export default Header;