import React, { Component } from 'react';

import './LeftNav.css';

class LeftNav extends Component {
    render () {
        return (
            <aside className="left-nav-bar">
                <a className="left-nav-bar-logo" href=""></a>
                <nav className="left-nav">
                    <a className="left-nav-item left-nav-item-1" href=""></a>
                    <a className="left-nav-item left-nav-item-2" href=""></a>
                    <a className="left-nav-item left-nav-item-3" href=""></a>
                    <a className="left-nav-item left-nav-item-4" href=""></a>
                    <a className="left-nav-item left-nav-item-5" href=""></a>                  
                </nav>
            </aside>
        )
    }
}

export default LeftNav;