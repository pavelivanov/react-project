import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import './ReportShortcuts.css';

class ReportShortcuts extends Component {
    render () {
        return (
            <Grid container className="mdc-layout-grid--custom-padding">
                <Grid item xs={6}>
                    <Typography variant="h6" gutterBottom>Report Shortcuts</Typography>
                </Grid>
                <Grid item xs={6} className="overview-mdc-layout-grid__cell--right">
                    <Button variant="contained" color="primary">
                        <Icon>add</Icon>
                         New Shortcut
                    </Button>                  
                </Grid>
                <Grid item xs={12}>
                       
                </Grid>
            </Grid>
        )
    }
}

export default ReportShortcuts;