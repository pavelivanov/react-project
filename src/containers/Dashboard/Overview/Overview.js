import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import './Overview.css';

class Overview extends Component {
    render () {
        return (
            <Grid container className="mdc-layout-grid--custom-padding">
                <Grid item xs={6}>
                    <Typography variant="h6" gutterBottom>Overview</Typography>
                </Grid>
                <Grid item xs={6} className="overview-mdc-layout-grid__cell--right">
                    <Button variant="contained" color="primary">
                        <Icon>add</Icon>
                         Create report
                    </Button>                  
                    <a className="theme-link" href="">Custom report type</a>   
                    <a className="theme-link" href="">Report settings</a>   
                </Grid>
                <Grid item xs={6}>
                    
                </Grid>
                <Grid item xs={6} className="overview-mdc-layout-grid__cell--right">
                       
                </Grid>
            </Grid>
        )
    }
}

export default Overview;