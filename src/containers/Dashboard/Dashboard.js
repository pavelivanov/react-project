import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import './Dashboard.css';
import LeftNav from '../../components/LeftNav/LeftNav';
import Header from '../../components/Header/Header';
import Overview from './Overview/Overview';
import ReportShortcuts from './ReportShortcuts/ReportShortcuts';

class Dashboard extends Component {
    render () {
        return (
            <div className="page-content-section">
                <LeftNav />
                <section className="main-content-section">
                    <Header />
                    <Switch>
                        <Route path='/overview' component={Overview}/>
                        <Route path='/report-shortcuts' component={ReportShortcuts}/>
                    </Switch>
                </section>
            </div>
        )
    }
}

export default Dashboard;